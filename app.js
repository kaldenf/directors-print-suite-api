var dotenv = require('dotenv').config({ silent: true })
var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')

// =======================
// configuration =========
// =======================
var app = express()
app.set('port', process.env.PORT || 8080)
mongoose.connect(process.env.DB)

app.use(function(req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*')

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'x-access-token, content-type, userid, funeralhomeid')

  next()
})

app.use(bodyParser.json())

// =======================
// routes ================
// =======================
app.options('*', function(req, res) {
  res.sendStatus(200)
})

require('./routes/asset-libraries')(app)
require('./routes/assets')(app)
require('./routes/cases')(app)
require('./routes/collections')(app)
require('./routes/funeralhomes')(app)
require('./routes/orders')(app)
require('./routes/packages')(app)
require('./routes/products')(app)
require('./routes/themes')(app)
require('./routes/theme-categories')(app)
require('./routes/users')(app)


app.listen(app.get('port'), function() {
  console.log('App started on port' + app.get('port'))
})
