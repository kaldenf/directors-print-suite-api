var auth = require('../utils/auth')
var collection = require('../models/theme-categories')

module.exports = function(app){
  app.post('/theme-categories', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, createThemeCategory)
  app.get('/theme-categories', auth.verifyToken, auth.verifyActiveUser, getThemeCategories)
  app.get('/theme-categories/:id', auth.verifyToken, auth.verifyActiveUser, getThemeCategory)
  app.delete('/theme-categories/:id', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, deleteThemeCategory)
}

function getThemeCategories (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getThemeCategory (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteThemeCategory (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createThemeCategory (req, res, next) {
  collection.create(req.body.data).then(function(response){
    res.json(response)
  })
}
