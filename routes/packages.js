var auth = require('../utils/auth')
var collection = require('../models/packages')

module.exports = function(app){
  app.post('/packages', auth.verifyToken, auth.verifyActiveUser, createPackage)
  app.get('/packages', auth.verifyToken, auth.verifyActiveUser, getPackages)
  app.get('/packages/:id', auth.verifyToken, auth.verifyActiveUser, getPackage)
  app.post('/packages/package/:id', auth.verifyToken, auth.verifyActiveUser, updatePackage)
  app.delete('/packages/:id', auth.verifyAdmin, auth.verifyAdmin, deletePackage)
}

function getPackages (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getPackage (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function updatePackage (req, res, next) {
  console.log(req.body.data)
  collection.update(req.params.id, req.body.data).then(
    res.json({success:true,message:'Success'})
  )
}

function deletePackage (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createPackage (req, res, next) {
  collection.create(req.body.data).then(
    res.json({message:'Successs'})
  )
}
