var auth = require('../utils/auth')
var collection = require('../models/collections')

module.exports = function(app){
  app.post('/collections', auth.verifyAdmin, createCollection)
  app.get('/collections', auth.verifyToken, auth.verifyActiveUser, getcollections)
  app.get('/collections/:id', auth.verifyToken, auth.verifyActiveUser, getCollection)
  app.delete('/collections/:id', auth.verifyAdmin, auth.verifyAdmin, deleteCollection)
}

function getcollections (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getCollection (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteCollection (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createCollection (req, res, next) {
  collection.create(req.body.data).then(
    res.json({message:'Successs'})
  )
}
