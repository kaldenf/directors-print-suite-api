var auth = require('../utils/auth')
var collection = require('../models/orders')
var pdf = require('../utils/html-pdf')

module.exports = function(app) {
	app.post('/orders', auth.verifyToken, auth.verifyActiveUser, createOrder)
	app.post('/orders/order/:id', auth.verifyToken, auth.verifyActiveUser, updateOrder)
	app.post('/orders/convert', pdf.convertHtml)
	app.get('/orders', auth.verifyToken, auth.verifyActiveUser, getOrders)
	app.get('/orders/:id', auth.verifyToken, auth.verifyActiveUser, getOrder)
	app.get('/orders/recent/:count', auth.verifyToken, auth.verifyActiveUser, getRecentOrders)
	app.delete('/orders/:id', auth.verifyAdmin, auth.verifyAdmin, deleteOrder)
}

function getRecentOrders(req, res, next) {
	collection.getRecent(req.params.count, req.headers.userid).then(function(data) {
		res.json(data)
	})
}

function getOrders(req, res, next) {
	collection.getAll(req.headers.userid).then(function(data) {
		res.json(data)
	})
}

function getOrder(req, res, next) {
	collection.get(req.params.id).then(function(data) {
		res.json(data)
	})
}

function deleteOrder(req, res, next) {
	collection.remove(req.params.id).then(
		res.json({
			success: true,
			message: 'Success'
		})
	)
}

function createOrder(req, res, next) {
	collection.create(req.body.data).then(function(order, err) {
		res.json(order.id)
	})
}

function updateOrder(req, res, next) {
	collection.update(req.params.id, req.body.data).then(function() {
		res.json({
			success: true,
			message: "Order updated"
		})
	})
}
