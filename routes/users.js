var auth = require('../utils/auth')
var collection = require('../models/users')
var passwordHash = require('password-hash')
var jwt = require('jsonwebtoken')
var emailer = require('../utils/emailer')
var randomstring = require('randomstring')
var iploc = require('../utils/iploc')

module.exports = function(app) {
  app.post('/users', checkEmail, createUser, sendToken)
	app.post('/users/checkemail', checkEmail, allClear)
  app.get('/users/iplocation', iploc.getIpLocation)
  app.post('/users/update/:id', auth.verifyAdmin, updateUser)
  app.post('/users/user/', auth.verifyToken, auth.verifyActiveUser, updateUser)
  app.post('/users/verify', verifyEmail, sendToken)
  app.post('/users/admin/verify', verifyEmail, auth.verifyAdmin, sendToken)
  app.post('/users/verifyToken', auth.verifyToken, auth.verifyActiveUser, validToken)
  app.post('/users/staff', auth.verifyManager, checkEmail, createStaff)
  app.post('/users/staff/activate', verifyActivationId, activateStaffMember, sendToken)
  app.post('/users/staff/resend-activation', resendActivation)
  app.get('/users', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, getUsers)
  app.get('/users/user/', auth.verifyToken, auth.verifyActiveUser, getUser)
  app.get('/users/staff', auth.verifyToken, auth.verifyActiveUser, getStaff)
  app.delete('/users/:id', auth.verifyManager, deleteUser)
}

function allClear(req, res, next) {
	res.json({
		success: true,
		message: 'Success'
	})
}

function checkEmail(req, res, next) {
  collection.verifyEmail(req.body.data.email).then(function(data) {
    if (data) {
      res.json({
        success: false,
        message: "Email already being used"
      })
    } else {
      req.body.user = data
      next()
    }
  })
}

function verifyEmail(req, res, next) {
  collection.verifyEmail(req.body.data.email).then(function(data) {
    if (data) {
      if (passwordHash.verify(req.body.data.password, data.password)) {
        req.user = data
        next()
      } else {
        res.json({
          success: false,
          message: "Password doesn't match"
        })
      }
    } else {
      res.json({
        success: false,
        message: "User not found"
      })
    }
  })
}

function sendToken(req, res, next) {
  var object = {
    email: req.body.data.email,
    password: req.body.data.password,
  }
  var token = jwt.sign(object, process.env['JWTSecret'], {
    expiresInMinutes: 1440 // expires in 24 hours
  })
  res.json({
    token: token,
    userId: req.user.id,
    funeralHomeId: req.user.funeralHomeId
  })
}

function getUsers(req, res, next) {
  collection.getAll().then(function(data) {
    res.json(data)
  })
}

function getUser(req, res, next) {
  collection.get(req.headers.userid).then(function(data) {
    res.json(data)
  })
}

function updateUser(req, res, next) {
  collection.update(req.headers.userid, req.body.data.user).then(function(data){
    res.json(data)
  })
}

function deleteUser(req, res, next) {
  collection.remove(req.params.id).then(
    res.json({
      success: true,
      message: 'Success'
    })
  )
}

function updateUser(req, res, next) {
  //TODO: verify no one is using email
  var user = {
    email: req.body.data.email,
    name: req.body.data.name
  }
  if(req.body.data.newPassword){
    var hashedPassword = passwordHash.generate(req.body.data.newPassword)
    user.password = hashedPassword
  }
  collection.update(req.headers.userid, user).then(function(response) {
    res.json({
      succes: true,
      message: 'Success'
    })
  })
}

function createUser(req, res, next) {
  if (req.body.data.admin || req.body.data.active || req.body.data.manager) {
    res.json({
      success: false,
      message: 'Not authorized'
    })
  } else {
    collection.create(req.body.data).then(function(user) {
      var data = {
        from: 'us@builtbyhq.com',
        to: 'kalden@builtbyhq.com',
        subject: 'Welcome to Bass Mollett',
        text: 'You have signed up to be part of the Bass Mollett Program, wait for further instructions'
      }
      emailer.sendEmail(data)
      req.user = user
      next()
    })
  }
}

function createStaff(req, res, next) {
  if (req.body.data.admin || req.body.data.active || req.body.data.manager) {
    res.json({
      success: false,
      message: 'Not authorized'
    })
  } else {
    req.body.data.funeralHomeId = req.headers.funeralhomeid
    req.body.data.activationId = randomstring.generate(10)
    collection.create(req.body.data).then(function(user) {
      var email = {
        from: 'us@builtbyhq.com',
        to: 'kalden@builtbyhq.com',
        subject: 'Welcome to Bass Mollett',
        text: 'You have been added as a staff member for ' + req.body.data.funeralHomeName + ' click this link to finish signing up http://localhost:8004/staff/' + req.body.data.activationId + ''
      }
      emailer.sendEmail(email)
      res.json({
        success: true,
        message: 'Staff Member Created'
      })
    })
  }
}

function resendActivation(req, res, next){
  var email = {
    from: 'us@builtbyhq.com',
    // to: req.body.data.email,
    to: 'kalden@builtbyhq.com',
    subject: 'Welcome to Bass Mollett',
    text: 'You have been added as a staff member for ' + req.body.data.funeralHomeName + ' click this link to finish signing up http://localhost:8004/staff/' + req.body.data.activationId + ''
  }
  emailer.sendEmail(email)
  res.json({
    success: true,
    message: 'Email resent'
  })
}

function getStaff(req, res, next) {
  collection.getStaff(req.headers.funeralhomeid).then(function(data) {
    res.json(data)
  })
}

function verifyActivationId(req, res, next) {
  collection.getStaffMemberByActivationId(req.body.data).then(function(data) {
    if (data) {
      req.user = data
      next()
    } else {
      res.json({
        success: false,
        message: 'No user found with that email'
      })
    }
  })
}

function activateStaffMember(req, res, next) {

  var user = {
    activationId: null,
    active: true,
  }
  var hashedPassword = passwordHash.generate(req.body.data.password)
  user.password = hashedPassword

  collection.update(req.user._id, user).then(function(user) {
    req.user = user
    next()
  })
}

function validToken(req, res, next) {
  res.json({
    success: true,
    message: 'token is valid'
  })
}
