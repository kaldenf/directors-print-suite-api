var auth = require('../utils/auth')
var collection = require('../models/funeralhomes')
var aws = require('../utils/aws.js')

module.exports = function(app){
  app.post('/funeral-homes', createfuneralHome)
  app.post('/funeral-homes/home', auth.verifyToken, auth.verifyActiveUser, auth.verifyManager, updatefuneralHome)
  app.get('/funeral-homes/sign-url', aws.getSignedUrl)
  app.get('/funeral-homes', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, getfuneralHomes)
  app.get('/funeral-homes/home', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, getfuneralHome)
  app.delete('/funeral-homes/:id', auth.verifyAdmin, auth.verifyAdmin, deletefuneralHome)
}

function getfuneralHomes (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getfuneralHome (req, res, next) {
  collection.get(req.headers.funeralhomeid).then(function(data){
    res.json(data)
  })
}

function deletefuneralHome (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createfuneralHome (req, res, next) {
  collection.create(req.body.data).then(function(data){
    res.json(data)
  })
}

function updatefuneralHome (req, res, next) {
  collection.update(req.headers.funeralhomeid, req.body.data).then(function(data){
    res.json(data)
  })
}
