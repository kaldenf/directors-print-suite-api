var auth = require('../utils/auth')
var collection = require('../models/asset-libraries')

module.exports = function(app){
  app.post('/asset-libraries', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, createAssetLibrary)
  app.get('/asset-libraries', auth.verifyToken, auth.verifyActiveUser, getAssetLibraries)
  app.get('/asset-libraries/:id', auth.verifyToken, auth.verifyActiveUser, getAssetLibrary)
  app.delete('/asset-libraries/:id', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, deleteAssetLibrary)
}

function getAssetLibraries (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getAssetLibrary (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteAssetLibrary (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createAssetLibrary (req, res, next) {
  collection.create(req.body.data).then(function(response){
    res.json(response)
  })
}
