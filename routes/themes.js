var auth = require('../utils/auth')
var collection = require('../models/themes')

module.exports = function(app) {
	app.post('/themes', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, createTheme)
	app.post('/theme/:id', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, updateTheme)
	app.get('/themes', auth.verifyToken, auth.verifyActiveUser, getThemes)
	app.get('/theme/:id', auth.verifyToken, auth.verifyActiveUser, getTheme)
	app.get('/themes/category/:id', auth.verifyToken, auth.verifyActiveUser, getCategoryThemes)
	app.delete('/themes/:id', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, deleteTheme)
}

function getThemes(req, res, next) {
	collection.getAll().then(function(data) {
		res.json(data)
	})
}

function getTheme(req, res, next) {
	collection.get(req.params.id).then(function(data) {
		res.json(data)
	})
}

function getCategoryThemes(req, res, next) {
	collection.getCategoryThemes(req.params.id).then(function(data) {
		res.json(data)
	})
}

function deleteTheme(req, res, next) {
	collection.remove(req.params.id).then(
		res.json({
			success: true,
			message: 'Success'
		})
	)
}

function createTheme(req, res, next) {
	collection.create(req.body.data).then(
		res.json({
			message: 'Successs'
		})
	)
}

function updateTheme (req, res, next) {
  collection.update(req.params.id, req.body.data).then(function(data){
    res.json(data)
  })
}
