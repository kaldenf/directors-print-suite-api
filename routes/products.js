var auth = require('../utils/auth')
var collection = require('../models/products')

module.exports = function(app){
  app.post('/products', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, createProduct)
  app.get('/products', auth.verifyToken, auth.verifyActiveUser, getProducts)
  app.get('/products/:id', auth.verifyToken, auth.verifyActiveUser, getProduct)
  app.delete('/products/:id', auth.verifyAdmin, auth.verifyAdmin, deleteProduct)
}

function getProducts (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getProduct (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteProduct (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createProduct (req, res, next) {
  collection.create(req.body.data).then(
    res.json({message:'Successs'})
  )
}
