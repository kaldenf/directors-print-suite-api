var auth = require('../utils/auth')
var collection = require('../models/assets')

module.exports = function(app){
  app.post('/assets', auth.verifyToken, auth.verifyActiveUser, auth.verifyAdmin, createAsset)
  app.get('/assets', auth.verifyToken, auth.verifyActiveUser, getAssets)
  app.get('/assets/library/:id', auth.verifyToken, auth.verifyActiveUser, getLibraryAssets)
  app.get('/asset/:id', auth.verifyToken, auth.verifyActiveUser, getAsset)
  app.delete('/asset/:id', auth.verifyAdmin, auth.verifyAdmin, deleteAsset)
}

function getAssets (req, res, next) {
  collection.getAll().then(function(data){
    res.json(data)
  })
}

function getAsset (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteAsset (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createAsset (req, res, next) {
  collection.create(req.body.data).then(
    res.json({message:'created Success'})
  )
}

function getLibraryAssets(req, res, next) {
  collection.getLibraryAssets(req.params.id).then(function(data){
    res.json(data)
  })
}
