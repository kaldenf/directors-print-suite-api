var auth = require('../utils/auth')
var collection = require('../models/cases')
var aws = require('../utils/aws')

module.exports = function(app){
  app.post('/cases', auth.verifyToken, auth.verifyActiveUser, createCase)
  app.post('/cases/case/:id', auth.verifyToken, auth.verifyActiveUser, updateCase)
  app.get('/uploadcase/csv', auth.verifyToken, auth.verifyActiveUser, aws.getCSVSignedUrl)
  app.get('/cases', auth.verifyToken, auth.verifyActiveUser, getCases)
  app.get('/cases/:id', auth.verifyToken, auth.verifyActiveUser, getCase)
  app.delete('/cases/case/:id', auth.verifyAdmin, auth.verifyAdmin, deleteCase)
}

function getCases (req, res, next) {
  collection.getAll(req.headers.funeralhomeid).then(function(data){
    res.json(data)
  })
}

function getCase (req, res, next) {
  collection.get(req.params.id).then(function(data){
    res.json(data)
  })
}

function deleteCase (req, res, next) {
  collection.remove(req.params.id).then(
    res.json({success:true,message:'Success'})
  )
}

function createCase (req, res, next) {
  collection.create(req.headers.funeralhomeid).then(function(data){
    res.json(data)
  })
}

function updateCase (req, res, next) {
  collection.update(req.params.id, req.body.data).then(function(data){
    res.json(data)
  })
}
