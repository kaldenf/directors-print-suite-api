var iplocation = require('iplocation')

module.exports = {
  getIpLocation:getIpLocation
}

function getIpLocation(req, res, next) {
  var ip = req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;
  iplocation(ip, function(error, response){
    if(error){
      res.json(error)
    } else {
      res.json(response)
    }
  })
}
