var jwt = require('jsonwebtoken')
var users = require('../models/users')

module.exports = {
  verifyToken: verifyToken,
  verifyAdmin: verifyAdmin,
  verifyManager: verifyManager,
  verifyActiveUser: verifyActiveUser
}

function verifyToken(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, process.env.JWTSecret, function(err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: 'Failed to authenticate token.'
        })
      } else {
        next()
      }
    })
  } else {
    res.json({
      success: false,
      message: "No token given."
    })
  }
}

function verifyActiveUser(req, res, next){
  //if verifying for login
  if (req.user) {
    if(req.user.active == true){
      next()
    }
    else {
      res.json({
        success: true,
        active: false,
        message: "Inactive User"
      })
    }
  }
  //else verify for other requests
  users.get(req.headers.userid).then(function(data) {
    if (data) {
      if (data.active == true) {
        next()
      } else {
        res.json({
          success: true,
          active: false,
          message: "Inactive User"
        })
      }
    } else {
      res.json({
        success: true,
        active: false,
        message: "Inactive User"
      })
    }
  })
}

function verifyAdmin(req, res, next) {
  //if verifying for login
  if (req.user) {
    if(req.user.admin == true){
      next()
    }
    else {
      res.json({
        success: false,
        message: "Not authorized."
      })
    }
  }
  //else verify for other requests
  users.get(req.headers.userid).then(function(data) {
    if (data) {
      if (data.admin == true) {
        next()
      } else {
        res.json({
          success: false,
          message: "Not authorized."
        })
      }
    } else {
      res.json({
        success: false,
        message: "Not authorized."
      })
    }
  })
}

function verifyManager(req, res, next) {
  users.get(req.headers.userid).then(function(data) {
    if (data) {
      if (data.manager == true) {
        next()
      } else {
        res.json({
          success: false,
          message: "Not authorized."
        })
      }
    } else {
      res.json({
        success: false,
        message: "Not authorized."
      })
    }
  })
}
