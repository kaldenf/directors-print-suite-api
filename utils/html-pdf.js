var pdf = require('html-pdf')

module.exports = {
	convertHtml: convertHtml
}

function convertHtml(req, res, next) {
	var options = {
		"height": "11in",
		"width": "8.5in"
	}
	pdf.create(req.body.data.html, options).toStream(function(err, stream) {
		if (err) {
			res.json({
				success: false,
				message: "something went wrong!"
			})
		} else {
			stream.pipe(res)
		}
	})
}
