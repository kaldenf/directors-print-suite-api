'use strict'

var aws = require('aws-sdk')
var request = require('request')

module.exports = {
  getSignedUrl: getSignedUrl,
  getCSVSignedUrl: getCSVSignedUrl
}

var AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY
var AWS_SECRET_KEY = process.env.AWS_SECRET_KEY
var S3_BUCKET = process.env.S3_BUCKET

function getSignedUrl(req, res, next) {
  aws.config.update({
    accessKeyId: AWS_ACCESS_KEY,
    secretAccessKey: AWS_SECRET_KEY,
    signatureVersion: 'v4',
    region: 'us-west-2'
  })
  var s3 = new aws.S3()
  var s3_params = {
    Bucket: S3_BUCKET,
    Key: req.query.file_name,
    Expires: 60,
    ContentType: req.query.file_type,
    ACL: 'public-read'
  }
  s3.getSignedUrl('putObject', s3_params, function(err, data) {
    if (err) {
      console.log(err)
    } else {
      var return_data = {
        signed_request: data,
        file_name: req.query.file_name,
        file: req.body.file
      }
      res.write(JSON.stringify(return_data))
      res.end()
    }
  })
}

function getCSVSignedUrl(req, res, next) {
  aws.config.update({
    accessKeyId: AWS_ACCESS_KEY,
    secretAccessKey: AWS_SECRET_KEY,
    signatureVersion: 'v4',
    region: 'us-west-2'
  })
  var s3 = new aws.S3()
  var s3_params = {
    Bucket: S3_BUCKET,
    Key: req.query.file_name,
    Expires: 60,
    ContentType: req.query.file_type,
    ACL: 'public-read'
  }
  s3.getSignedUrl('putObject', s3_params, function(err, data) {
    if (err) {
      console.log(err)
    } else {
      var return_data = {
        signed_request: data,
        file_name: req.query.file_name,
        file: req.body.file
      }
      res.write(JSON.stringify(return_data))
      res.end()
    }
  })
}
