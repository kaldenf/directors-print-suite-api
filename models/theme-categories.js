'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var themeCategoriesSchema = mongoose.Schema({
  name: String,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
themeCategoriesSchema.statics = _.assign(themeCategoriesSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll
})

function create(object) {
  var newThemeCategory = new themeCategoriesModel(object)
  return newThemeCategory.save()
}

function remove(id) {
  return themeCategoriesModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return themeCategoriesModel.findOne({
    _id: id
  })
}

function getAll() {
  return themeCategoriesModel.find()
}

var themeCategoriesModel = mongoose.model('themeCategories', themeCategoriesSchema)
module.exports = themeCategoriesModel
