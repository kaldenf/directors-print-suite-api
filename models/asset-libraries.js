'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')


/**
 * Schema
 */
var assetLibrariesSchema = mongoose.Schema({
  name: String,
  created: {
    type: Date,
    default: Date.now
  }
})


/**
 * methods
 */
assetLibrariesSchema.statics = _.assign(assetLibrariesSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll
})

function create(object) {
  var newAssetLibrary = new assetLibrariesModel(object)
  return newAssetLibrary.save()
}

function remove(id) {
  return assetLibrariesModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return assetLibrariesModel.findOne({
    _id: id
  })
}

function getAll() {
  return assetLibrariesModel.find()
}

var assetLibrariesModel = mongoose.model('assetLibrary', assetLibrariesSchema)
module.exports = assetLibrariesModel
