'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var packagesSchema = mongoose.Schema({
	name: String,
	description: String,
	thumbnail: String,
	products: Array,
	collectionId: String,
	created: {
		type: Date,
		default: Date.now
	}
})

/**
 * methods
 */
packagesSchema.statics = _.assign(packagesSchema.statics, {
	create: create,
	remove: remove,
	get: get,
	getAll: getAll
})

function create(object) {
	var newPackage = new packageModel(object)
	return newPackage.save()
}

function remove(id) {
	return packageModel.findOneAndRemove({
		_id: id
	})
}

function update(id, updatedPackage) {
	return packageModel.findByIdAndUpdate(id, updatedPackage)
}

function get(id) {
	return packageModel.findOne({
		_id: id
	})
}

function getAll() {
	return packageModel.find()
}

var packageModel = mongoose.model('packages', packagesSchema)
module.exports = packageModel
