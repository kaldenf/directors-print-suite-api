'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')
var passwordHash = require('password-hash')

/**
 * Schema
 */
var usersModel = mongoose.Schema({
  funeralHomeId: String,
  name: String,
  email: String,
  password: String,
  active: Boolean,
  manager: Boolean,
  admin: Boolean,
  activationId: String,
	details: Object,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
usersModel.statics = _.assign(usersModel.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  getStaff: getStaff,
  verifyEmail: verifyEmail,
  update: update,
  getStaffMemberByActivationId: getStaffMemberByActivationId
})

function verifyEmail(email) {
  return usersModel.findOne({
    email: email
  })
}

function create(object) {
  if(object.password){
    var hashedPassword = passwordHash.generate(object.password)
    object.password = hashedPassword
  }
	if(!object.active){
		object.active = false
	}
  var newUser = new usersModel(object)
  return newUser.save()
}

function remove(id) {
  return usersModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return usersModel.findOne({
    _id: id
  })
}

function getAll() {
  return usersModel.find()
}

function update(id, user) {
  return usersModel.findByIdAndUpdate(id, user)
}

function getStaff(id) {
  return usersModel.find({
    funeralHomeId: id
  })
}

function getStaffMemberByActivationId(user) {
  return usersModel.findOne({
    activationId: user.activationId,
    email: user.email
  })
}

var usersModel = mongoose.model('users', usersModel)
module.exports = usersModel
