'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var collectionsSchema = mongoose.Schema({
  name: String,
  description: String,
  logo: String,
  background: String,
  products: Array,
  packages: Array,
  themes: Array,
  active: Boolean,
  paperType: String,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
collectionsSchema.statics = _.assign(collectionsSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll
})

function create(object) {
  var newCollection = new collectionsModel(object)
  return newCollection.save()
}

function remove(id) {
  return collectionsModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return collectionsModel.findOne({
    _id: id
  })
}

function getAll() {
  return collectionsModel.find({})
}

var collectionsModel = mongoose.model('collections', collectionsSchema)
module.exports = collectionsModel
