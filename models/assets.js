'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var assetsSchema = mongoose.Schema({
  libraryId: String,
  url: String,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
assetsSchema.statics = _.assign(assetsSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  getLibraryAssets: getLibraryAssets
})

function create(object) {
  var newAsset = new assetsModel(object)
  return newAsset.save()
}

function remove(id) {
  return assetsModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return assetsModel.findOne({
    _id: id
  })
}

function getAll() {
  return assetsModel.find()
}

function getLibraryAssets(id) {
  return assetsModel.find({libraryId:id})
}

var assetsModel = mongoose.model('assets', assetsSchema)
module.exports = assetsModel
