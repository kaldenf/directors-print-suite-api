'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var productsSchema = mongoose.Schema({
  name: String,
  thumbnail: String,
  archived: Boolean,
  layouts: Array,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
productsSchema.statics = _.assign(productsSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  getList: getList
})

function create(object) {
  var newProduct = new productsModel(object)
  return newProduct.save()
}

function remove(id) {
  return productsModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return productsModel.findOne({
    _id: id
  })
}

function getAll() {
  return productsModel.find()
}

function getList(ids) {
  for (var i in ids) {
    //TODO find each product and return array of products
  }
}

var productsModel = mongoose.model('products', productsSchema)
module.exports = productsModel
