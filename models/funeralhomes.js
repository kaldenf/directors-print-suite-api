'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var funeralHomeSchema = mongoose.Schema({
  name: String,
  logo: String,
  active: Boolean,
  collections: Array,
  customerId: String,
  created: {
    type: Date,
    default: Date.now
  }
})


/**
 * methods
 */
funeralHomeSchema.statics = _.assign(funeralHomeSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  update: update
})

function create(object) {
  var newFuneralHome = new funeralHomeModel(object)
  return newFuneralHome.save()
}

function remove(id) {
  return funeralHomeModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return funeralHomeModel.findOne({
    _id: id
  })
}

function getAll() {
  return funeralHomeModel.find({})
}

function update(id, home) {
  return funeralHomeModel.findByIdAndUpdate(id, home)
}

var funeralHomeModel = mongoose.model('funeralHome', funeralHomeSchema)
module.exports = funeralHomeModel
