'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection(process.env.DB)

autoIncrement.initialize(connection)

/**
 * Schema
 */
var casesSchema = mongoose.Schema({
  funeralHomeId: String,
  details: Object,
  caseId: {
    type: Number,
    ref: 'caseId'
  },
  complete: Boolean,
  archived: Boolean,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
casesSchema.statics = _.assign(casesSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  update: update
})

function create(id) {
  var object = {
    funeralHomeId: id
  }
  var newCase = new casesModel(object)
  return newCase.save()
}

function remove(id) {
  return casesModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return casesModel.findOne({
    _id: id
  })
}

function getAll(id) {
  return casesModel.find({funeralHomeId:id})
}

function update(id, updatedCase) {
  return casesModel.findByIdAndUpdate(id, updatedCase)
}

casesSchema.plugin(autoIncrement.plugin, { model: 'cases', field: 'caseId' })
var casesModel = mongoose.model('cases', casesSchema)
module.exports = casesModel
