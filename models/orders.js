'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var ordersSchema = mongoose.Schema({
  creatorId: String,
  caseId: String,
  theme: Object,
  collections: Array,
  archived: Boolean,
  created: {
    type: Date,
    default: Date.now
  }
})

/**
 * methods
 */
ordersSchema.statics = _.assign(ordersSchema.statics, {
  create: create,
  remove: remove,
  get: get,
  getAll: getAll,
  getRecent: getRecent,
  update: update
})

function create(object) {
  var newOrder = new ordersModel(object)
  return newOrder.save()
}

function remove(id) {
  return ordersModel.findOneAndRemove({
    _id: id
  })
}

function get(id) {
  return ordersModel.findOne({
    _id: id
  })
}

function getRecent(count, id) {
  return ordersModel.find({creatorId:id}).sort('-date').limit(count)
}

function getAll(id) {
  return ordersModel.find({creatorId:id})
}

function update(id, updatedOrder) {
  return ordersModel.findByIdAndUpdate(id, updatedOrder)
}

var ordersModel = mongoose.model('orders', ordersSchema)
module.exports = ordersModel
