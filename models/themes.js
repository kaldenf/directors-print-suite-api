'use strict'

/**
 * dependencies
 */
var mongoose = require('mongoose')
var _ = require('lodash')

/**
 * Schema
 */
var themesSchema = mongoose.Schema({
	name: String,
	image: String,
	category: String,
	collections: Array,
	created: {
		type: Date,
		default: Date.now
	}
})

/**
 * methods
 */
themesSchema.statics = _.assign(themesSchema.statics, {
	create: create,
	remove: remove,
	get: get,
	getAll: getAll,
	update: update,
	getCategoryThemes: getCategoryThemes
})

function create(object) {
	var newTheme = new themesModel(object)
	return newTheme.save()
}

function remove(id) {
	return themesModel.findOneAndRemove({
		_id: id
	})
}

function get(id) {
	return themesModel.findOne({
		_id: id
	})
}

function getAll() {
	return themesModel.find()
}

function getCategoryThemes(categoryId) {
	return themesModel.find({
		category: categoryId
	})
}

function update(id, updatedTheme) {
  return themesModel.findByIdAndUpdate(id, updatedTheme)
}

var themesModel = mongoose.model('themes', themesSchema)
module.exports = themesModel
